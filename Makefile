dc=docker-compose

reup: down up

build: build-node

run-sh:
	$(dc) -p 8081:8080 run node sh

build-node:
	docker build ./docker/node/ -t poster-ui/node:latest --no-cache

sh:
	docker-compose exec node sh

up:
	$(dc) up -d

ups:
	$(dc) up

down:
	$(dc) down
