import Vue from 'vue';
import VueRouter from 'vue-router';
import ContentAdministrationPage from '../Pages/ContentAdministrationPage.vue';
import {store} from "@/Store/Store";
import UpdateUserToken from "@/Store/Actions/UpdateUserToken";

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'content-administration',
        component: ContentAdministrationPage,
    }
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.query.userToken !== undefined) {
        store.dispatch(UpdateUserToken.Key, to.query.userToken.toString());
        delete to.query.userToken;
        router.replace({query: to.query});
    }

    next();
});

export default router;
