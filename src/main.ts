import Vue from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from './Router/Router';
import {store} from './Store/Store';
import "reflect-metadata";
import vuetify from '@/Plugins/Vuetify';
import InitializeState from "@/Store/Actions/InitializeState";
import "emoji-mart-vue-fast/css/emoji-mart.css";
import 'vue2-datepicker/index.css';

Vue.config.productionTip = false;

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App),
    beforeCreate(): void {
        this.$store.dispatch(InitializeState.Key);
    },
}).$mount('#app');
