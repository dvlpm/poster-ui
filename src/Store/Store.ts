import Vue from 'vue';
import Vuex, {MutationPayload, StoreOptions} from 'vuex';
import {initialState, State} from "@/Store/State";
import mutations from "@/Store/Mutations";
import actions from "@/Store/Actions";
import getters from "@/Store/Getters";

Vue.use(Vuex);

export const store = new Vuex.Store<State>({
    state: initialState(),
    mutations: mutations(),
    actions: actions(),
    getters: getters()
});

store.subscribe((mutation: MutationPayload, state: State): void => {
    let storableStore = {
        version: state.version,
        user: {
            token: state.user.token
        },
        selectedChannel: state.selectedChannel,
        postListLimit: state.postListLimit,
        postGenerator: state.postGenerator
    };

    localStorage.setItem('state', JSON.stringify(storableStore));
});
