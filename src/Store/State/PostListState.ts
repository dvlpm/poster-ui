import Post from "@/App/Model/Post";

// TODO use it? or not?
export default class PostListState {
    public posts: Post[] = [];
    public limit: number = 10;
    public index: number = 0;
    public isAllLoaded: boolean = false;

    constructor(posts: Post[] = [], limit: number = 10, index: number = 0) {
        this.posts = posts;
        this.limit = limit;
        this.index = index;
    }
}
