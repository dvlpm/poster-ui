import {MutationTree} from "vuex";
import {State} from "@/Store/State";
import Post from "@/App/Model/Post";
import Channel from "@/App/Model/Channel";

export enum Mutations {
    stateInitialized = 'stateInitialized',
    userTokenUpdated = 'userTokenUpdated',
    selectedChannelUpdated = 'selectedChannelUpdated',
    postsUpdated = 'postsUpdated',
    postsAppended = 'postsAppended',
    allPostsLoaded = 'allPostsLoaded',
    channelListUpdated = 'channelListUpdated',
    generatedPostsUpdated = 'generatedPostsUpdated',
    isPostsSortingEnabledUpdated = 'isPostsSortingEnabledUpdated',
}

export default (): MutationTree<State> => {
    return {
        [Mutations.stateInitialized](state: State, sourceState: State): void {
            this.replaceState(Object.assign(state, sourceState));
        },
        [Mutations.userTokenUpdated](state: State, userToken: string): void {
            state.user.token = userToken;
        },
        [Mutations.selectedChannelUpdated](state: State, selectedChannel: Channel): void {
            state.selectedChannel = selectedChannel;
        },
        [Mutations.postsUpdated](state: State, posts: Post[]): void {
            state.isAllPostsLoaded = false;
            state.posts = posts;
        },
        [Mutations.postsAppended](state: State, posts: Post[]): void {
            state.posts.push(...posts);
        },
        [Mutations.allPostsLoaded](state: State): void {
            state.isAllPostsLoaded = true;
        },
        [Mutations.channelListUpdated](state: State, channels: Channel[]): void {
            state.channels = channels;
        },
        [Mutations.generatedPostsUpdated](state: State, posts: Post[]): void {
            state.postGenerator.posts = posts;
        },
        [Mutations.isPostsSortingEnabledUpdated](state: State, isEnabled: boolean): void {
            state.isPostsSortingEnabled = isEnabled;
        },
    }
}
