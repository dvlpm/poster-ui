import Post from "@/App/Model/Post";
import Channel from "@/App/Model/Channel";
import PostGeneratorState from "@/Store/State/PostGeneratorState";
import PostListState from "@/Store/State/PostListState";

export interface State {
    version: string,
    user: {
        token: string
    },
    posts: Post[],
    isPostsSortingEnabled: boolean,
    isAllPostsLoaded: boolean,
    channels: Channel[],
    selectedChannel: Channel | null,
    postListLimit: number,
    postGenerator: PostGeneratorState,
}

export const initialState = (): State => {
    return {
        version: '',
        user: {
            token: ''
        },
        posts: [],
        isPostsSortingEnabled: true,
        isAllPostsLoaded: false,
        channels: [],
        selectedChannel: null,
        postListLimit: 10,
        postGenerator: new PostGeneratorState(),
    }
};
