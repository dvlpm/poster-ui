import {ActionTree, Commit, Dispatch} from "vuex";
import {Getters} from "@/Store/Getters";
import {State} from "@/Store/State";
import {container} from "tsyringe";
import UpdateUserToken from "@/Store/Actions/UpdateUserToken";
import InitializeState from "@/Store/Actions/InitializeState";
import GetPostList from "@/Store/Actions/GetPostList";
import GetChannelList from "@/Store/Actions/GetChannelList";
import UpdateSelectedChannel from "@/Store/Actions/UpdateSelectedChannel";
import LoadMorePosts from "@/Store/Actions/LoadMorePosts";
import SavePost from "@/Store/Actions/SavePost";
import UploadMedia from "@/Store/Actions/UploadMedia";
import UpdateGeneratedPosts from "@/Store/Actions/UpdateGeneratedPosts";
import AppendPosts from "@/Store/Actions/AppendPosts";
import ReloadPosts from "@/Store/Actions/ReloadPosts";
import UpdatePosts from "@/Store/Actions/UpdatePosts";
import UpdateIsPostsSortingEnabled from "@/Store/Actions/UpdateIsPostsSortingEnabled";

export interface ActionInjection {
    readonly dispatch: Dispatch
    readonly getters: Getters
    readonly commit: Commit
    readonly state: State
}

export default (): ActionTree<State, State> => {
    return {
        [InitializeState.Key]: container.resolve(InitializeState),
        [UpdateUserToken.Key]: container.resolve(UpdateUserToken),
        [UpdateSelectedChannel.Key]: container.resolve(UpdateSelectedChannel),
        [GetPostList.Key]: container.resolve(GetPostList),
        [GetChannelList.Key]: container.resolve(GetChannelList),
        [LoadMorePosts.Key]: container.resolve(LoadMorePosts),
        [SavePost.Key]: container.resolve(SavePost),
        [UploadMedia.Key]: container.resolve(UploadMedia),
        [UpdateGeneratedPosts.Key]: container.resolve(UpdateGeneratedPosts),
        [UpdatePosts.Key]: container.resolve(UpdatePosts),
        [AppendPosts.Key]: container.resolve(AppendPosts),
        [ReloadPosts.Key]: container.resolve(ReloadPosts),
        [UpdateIsPostsSortingEnabled.Key]: container.resolve(UpdateIsPostsSortingEnabled),
    }
}
