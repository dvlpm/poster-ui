import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {inject, injectable} from "tsyringe";
import PostService, {PostOrderByColumn} from "@/App/Service/PostService";
import OrderBy, {OrderByDirection} from "@/App/Dto/OrderBy";

@injectable()
export default class GetPostList implements ActionObject<State, State> {
    static readonly Key = 'getPostList';

    constructor(
        @inject(PostService) private postService: PostService
    ) {
    }

    handler: ActionHandler<State, State> = async (
        actionInjection: ActionInjection,
        getPostListPayload: GetPostListPayload
    ) => {
        return await this.postService.list(
            getPostListPayload.channelId,
            getPostListPayload.limit,
            getPostListPayload.offset,
            getPostListPayload.orderBy,
            actionInjection.getters.userToken
        );
    }
}

export class GetPostListPayload {
    public channelId: number;
    public limit: number;
    public offset: number;
    public orderBy: OrderBy<PostOrderByColumn>[];

    constructor(
        channelId: number,
        limit: number,
        offset: number = 0,
        orderBy = [
            new OrderBy(PostOrderByColumn.scheduledAt, OrderByDirection.DESC),
            new OrderBy(PostOrderByColumn.id, OrderByDirection.DESC),
        ]
    ) {
        this.channelId = channelId;
        this.limit = limit;
        this.offset = offset;
        this.orderBy = orderBy;
    }
}
