import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";
import Post from "@/App/Model/Post";

@injectable()
export default class UpdatePosts implements ActionObject<State, State> {
    static readonly Key = 'updatePosts';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, posts: Post[]) => {
        actionInjection.commit(Mutations.postsUpdated, posts);
    }
}
