import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";
import GetPostList, {GetPostListPayload} from "@/Store/Actions/GetPostList";

@injectable()
export default class LoadMorePosts implements ActionObject<State, State> {
    static readonly Key = 'loadMorePosts';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        if (actionInjection.state.selectedChannel === null
            || actionInjection.state.posts.length === 0
            || actionInjection.state.isAllPostsLoaded
        ) {
            return;
        }

        const posts = await actionInjection.dispatch(GetPostList.Key, new GetPostListPayload(
            actionInjection.state.selectedChannel.id,
            actionInjection.state.postListLimit,
            actionInjection.state.posts.length
        ));

        if (posts.length === 0) {
            actionInjection.commit(Mutations.allPostsLoaded);

            return;
        }

        actionInjection.commit(Mutations.postsAppended, posts);
    }
}
