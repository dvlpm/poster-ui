import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {inject, injectable} from "tsyringe";
import ChannelService from "@/App/Service/ChannelService";

@injectable()
export default class GetChannelList implements ActionObject<State, State> {
    static readonly Key = 'getChannelList';

    constructor(
        @inject(ChannelService) private channelService: ChannelService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        let channels = await this.channelService.list(actionInjection.getters.userToken);

        actionInjection.commit(Mutations.channelListUpdated, channels);
    }
}