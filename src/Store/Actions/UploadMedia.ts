import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {inject, injectable} from "tsyringe";
import MediaService from "@/App/Service/MediaService";

@injectable()
export default class UploadMedia implements ActionObject<State, State> {
    static readonly Key = 'uploadMedia';

    constructor(
        @inject(MediaService) private mediaService: MediaService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, payload: UploadMediaPayload) => {
        return this.mediaService.create(
            payload.file,
            actionInjection.getters.userToken
        );
    }
}

export class UploadMediaPayload {
    public file: File;

    constructor(file: File) {
        this.file = file;
    }
}
