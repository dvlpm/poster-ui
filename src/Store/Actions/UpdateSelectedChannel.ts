import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import Channel from "@/App/Model/Channel";
import {injectable} from "tsyringe";
import GetPostList, {GetPostListPayload} from "@/Store/Actions/GetPostList";

@injectable()
export default class UpdateSelectedChannel implements ActionObject<State, State> {
    static readonly Key = 'updateSelectedChannel';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, selectedChannel: Channel) => {
        const posts = await actionInjection.dispatch(GetPostList.Key, new GetPostListPayload(
            selectedChannel.id,
            actionInjection.state.postListLimit
        ));

        actionInjection.commit(Mutations.selectedChannelUpdated, selectedChannel);
        actionInjection.commit(Mutations.postsUpdated, posts);
    }
}
