import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";
import GetPostList, {GetPostListPayload} from "@/Store/Actions/GetPostList";

@injectable()
export default class ReloadPosts implements ActionObject<State, State> {
    static readonly Key = 'reloadPosts';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        if (actionInjection.state.selectedChannel === null) {
            return;
        }

        const posts = await actionInjection.dispatch(GetPostList.Key, new GetPostListPayload(
            actionInjection.state.selectedChannel.id,
            actionInjection.state.postListLimit
        ));

        actionInjection.commit(Mutations.postsUpdated, posts);
    }
}
