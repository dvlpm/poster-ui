import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";
import Post from "@/App/Model/Post";
import OrderBy from "@/App/Dto/OrderBy";
import {PostOrderByColumn} from "@/App/Service/PostService";

@injectable()
export default class SortPosts implements ActionObject<State, State> {
    static readonly Key = 'appendPost';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, orderBy: OrderBy<PostOrderByColumn>) => {
        const posts = actionInjection.getters
    }
}
