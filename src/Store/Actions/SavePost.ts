import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {inject, injectable} from "tsyringe";
import PostService from "@/App/Service/PostService";
import Post from "@/App/Model/Post";

@injectable()
export default class SavePost implements ActionObject<State, State> {
    static readonly Key = 'savePost';

    constructor(
        @inject(PostService) private postService: PostService
    ) {
    }

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, post: Post) => {
        if (actionInjection.state.selectedChannel === null) {
            return;
        }

        if (post.id === undefined) {
            return await this.postService.save(
                actionInjection.state.selectedChannel.id,
                post,
                actionInjection.getters.userToken
            );
        } else {
            return await this.postService.update(
                actionInjection.state.selectedChannel.id,
                post.id,
                post,
                actionInjection.getters.userToken
            );
        }
    }
}