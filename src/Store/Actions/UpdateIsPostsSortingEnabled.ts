import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";

@injectable()
export default class UpdateIsPostsSortingEnabled implements ActionObject<State, State> {
    static readonly Key = 'updateIsPostsSortingEnabled';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, isEnabled: boolean) => {
        actionInjection.commit(Mutations.isPostsSortingEnabledUpdated, isEnabled);
    }
}
