import {ActionHandler, ActionObject} from "vuex";
import {initialState, State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";
import GetPostList, {GetPostListPayload} from "@/Store/Actions/GetPostList";
import GetChannelList from "@/Store/Actions/GetChannelList";
import PostFactory from "@/App/Factory/PostFactory";

@injectable()
export default class InitializeState implements ActionObject<State, State> {
    static readonly Key = 'initializeState';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection) => {
        let state: State = initialState();

        if (localStorage.getItem('state') !== null) {
            state = JSON.parse(<string>localStorage.getItem('state'));
            state.postGenerator.posts = PostFactory.createArray(state.postGenerator.posts);
        }

        actionInjection.commit(Mutations.stateInitialized, state);

        if (!actionInjection.getters.isAuthenticated) {
            return;
        }

        await actionInjection.dispatch(GetChannelList.Key);

        if (state.selectedChannel === null) {
            return;
        }

        const posts = await actionInjection.dispatch(GetPostList.Key, new GetPostListPayload(
            state.selectedChannel.id,
            state.postListLimit
        ));

        if (posts.length === 0) {
            actionInjection.commit(Mutations.allPostsLoaded);
        }

        actionInjection.commit(Mutations.postsUpdated, posts);
    }
}
