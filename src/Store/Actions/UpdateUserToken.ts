import {ActionHandler, ActionObject} from "vuex";
import {State} from "@/Store/State";
import {ActionInjection} from "@/Store/Actions";
import {Mutations} from "@/Store/Mutations";
import {injectable} from "tsyringe";

@injectable()
export default class UpdateUserToken implements ActionObject<State, State> {
    static readonly Key = 'updateUserToken';

    handler: ActionHandler<State, State> = async (actionInjection: ActionInjection, userToken: string) => {
        actionInjection.commit(Mutations.userTokenUpdated, userToken);
    }
}