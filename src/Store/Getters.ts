import {State} from "@/Store/State";
import {GetterTree} from "vuex";
import Post from "@/App/Model/Post";
import PostSorterHelper from "@/App/Helper/PostSorterHelper";

export interface GettersInterface extends GetterTree<State, State> {
    userToken(...params: any): string;

    sortedPosts(...params: any): Post[];

    generatedPosts(...params: any): Post[];

    isAuthenticated(...params: any): boolean;
}

export type Getters = {
    readonly [P in keyof GettersInterface]: ReturnType<GettersInterface[P]>;
};

export default (): GettersInterface => {
    return {
        isAuthenticated(state: State): boolean {
            return state.user.token !== '';
        },
        userToken(state: State): string {
            return state.user.token;
        },
        sortedPosts(state: State): Post[] {
            if (!state.isPostsSortingEnabled) {
                return state.posts;
            }

            return state.posts.sort((a: Post, b: Post) => {
                return PostSorterHelper.sort(a, b);
            });
        },
        generatedPosts(state: State): Post[] {
            return state.postGenerator.posts;
        }
    }
}
