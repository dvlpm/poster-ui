import OrderBy from "@/App/Dto/OrderBy";

export default class OrderByHelper {
    public static makeArray<OrderByColumn>(orderBy: OrderBy<OrderByColumn>[]): Array<string> {
        let orderByArray: any = [];

        for (let order of orderBy) {
            orderByArray[order.column] = order.direction;
        }

        return orderByArray;
    }
}
