import Post from "@/App/Model/Post";

export default class PostSorterHelper {
    public static sort(a: Post, b: Post): number {
        if (a.scheduledAtAsDate === null && b.scheduledAtAsDate === null) {
            if (a.id === undefined && b.id === undefined) {
                return 0;
            }

            if (a.id === undefined && b.id !== undefined) {
                return -1;
            }

            if (a.id !== undefined && b.id === undefined) {
                return 1;
            }

            // @ts-ignore
            if (a.id < b.id) {
                return 1;
            }

            return -1;
        }

        if (a.scheduledAtAsDate === null && b.scheduledAtAsDate !== null) {
            return -1;
        }

        if (a.scheduledAtAsDate !== null && b.scheduledAtAsDate === null) {
            return 1;
        }

        // @ts-ignore
        if (a.scheduledAtAsDate < b.scheduledAtAsDate) {
            return 1;
        }

        return -1;
    }
}
