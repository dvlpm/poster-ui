import {EmojiIndex} from "emoji-mart-vue-fast";
import data from "@/Assets/emoji/all.json";

export default class EmojiHelper {
    static readonly emojiIndex: EmojiIndex = new EmojiIndex(data);
}
