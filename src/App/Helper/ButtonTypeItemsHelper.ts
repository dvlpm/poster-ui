export default class ButtonTypeItemsHelper {
    public static buttonTypeItems: ButtonTypeItem[] = [
        {
            text: 'reaction',
            value: 0,
        },
        {
            text: 'link',
            value: 1,
        }
    ];

    public static getByText(text: string | null): ButtonTypeItem | null {
        return this.buttonTypeItems.find((item: ButtonTypeItem) => {
            return item.text === text;
        }) ?? null;
    }
}

export interface ButtonTypeItem {
    text: string,
    value: number
}
