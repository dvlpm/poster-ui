import AbstractModel from "@/App/Model/AbstractModel";

interface Type<T> extends Function {
    new(...args: any[]): T;
}

export type TypedModelFactory<T extends AbstractModel> = (object: object) => T;

export default class ModelFactory {
    public static create<T extends AbstractModel>(type: Type<T>, object: object): T {
        return ModelFactory.createUsingFactory(() => new type(), object);
    }

    public static createArray<T extends AbstractModel>(type: Type<T>, objects: object[]): T[] {
        return objects.map((object: object) => {
            return ModelFactory.create(type, object);
        });
    }

    public static createUsingFactory<T extends AbstractModel>(factory: TypedModelFactory<T>, object: object): T {
        const model = factory(object) as T;
        Object.assign(model, object);

        return model;
    }
}
