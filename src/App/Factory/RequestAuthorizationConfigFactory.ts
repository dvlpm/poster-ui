export default class RequestAuthorizationConfigFactory {
    public create(token: string): RequestAuthorizationConfigInterface {
        return {
            headers: {
                "Authorization": "Bearer " + token
            }
        } as RequestAuthorizationConfigInterface;
    }
}

export interface RequestAuthorizationConfigInterface {
    headers: object
}
