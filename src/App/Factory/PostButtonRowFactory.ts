import ModelFactory from "@/App/Factory/ModelFactory";
import PostButtonRow from "@/App/Model/PostButtonRow";
import PostButtonFactory from "@/App/Factory/PostButtonFactory";
import PostButton from "@/App/Model/PostButton";

export default class PostButtonRowFactory {
    public static create(object: object): PostButtonRow {
        let postButtonRow = ModelFactory.create<PostButtonRow>(PostButtonRow, object);

        postButtonRow.postButtons = PostButtonFactory.createArray(postButtonRow.postButtons);

        return postButtonRow;
    }

    public static createArray(objects: object[]): PostButtonRow[] {
        return objects.map((object: object) => {
            return PostButtonRowFactory.create(object);
        });
    }

    public static createWithButtons(...postButtons: PostButton[]): PostButtonRow {
        const postButtonRow = new PostButtonRow();

        postButtonRow.postButtons.push(...postButtons);

        return postButtonRow;
    }
}
