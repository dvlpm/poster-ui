import Post from "@/App/Model/Post";
import ModelFactory from "@/App/Factory/ModelFactory";
import PostButtonRowFactory from "@/App/Factory/PostButtonRowFactory";
import MediaFactory from "@/App/Factory/MediaFactory";

export default class PostFactory {
    public static create(object: object): Post {
        let post = ModelFactory.create<Post>(Post, object);

        post.postButtonRows = PostButtonRowFactory.createArray(post.postButtonRows);
        post.medias = MediaFactory.createArray(post.medias);

        return post;
    }

    public static createArray(objects: object[]): Post[] {
        return objects.map((object: object) => {
            return PostFactory.create(object);
        });
    }
}