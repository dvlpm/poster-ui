import ModelFactory from "@/App/Factory/ModelFactory";
import PostButton from "@/App/Model/PostButton";
import ButtonType from "@/App/Model/ButtonType";

export default class PostButtonFactory {
    public static create(object: object): PostButton {
        return ModelFactory.create<PostButton>(PostButton, object);
    }

    public static createArray(objects: object[]): PostButton[] {
        return objects.map((object: object) => {
            return PostButtonFactory.create(object);
        });
    }

    public static createEmpty(): PostButton {
        const button = new PostButton();
        button.type = new ButtonType('reaction');

        return button;
    }
}
