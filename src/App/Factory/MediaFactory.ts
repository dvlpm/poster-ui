import Media from "@/App/Model/Media";
import ModelFactory from "@/App/Factory/ModelFactory";

export default class MediaFactory {
    public static create(object: object): Media {
        return ModelFactory.create<Media>(Media, object);
    }

    public static createArray(objects: object[]): Media[] {
        return objects.map((object: object) => {
            return MediaFactory.create(object);
        });
    }
}