import Channel from "@/App/Model/Channel";
import ModelFactory from "@/App/Factory/ModelFactory";

export default class ChannelFactory {
    public static create(object: object): Channel {
        return ModelFactory.create<Channel>(Channel, object);
    }

    public static createArray(objects: object[]): Channel[] {
        return objects.map((object: object) => {
            return ChannelFactory.create(object);
        });
    }
}