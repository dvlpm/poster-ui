import AbstractModel from "@/App/Model/AbstractModel";

export default class Media extends AbstractModel {
    id: number;
    url: string;

    constructor(id: number, url: string) {
        super();
        this.id = id;
        this.url = url;
    }
}
