import PostButton from "@/App/Model/PostButton";
import AbstractModel from "@/App/Model/AbstractModel";

export default class PostButtonRow extends AbstractModel {
    public postButtons: Array<PostButton> = [];
}
