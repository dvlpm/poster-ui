import PostButtonRow from "@/App/Model/PostButtonRow";
import AbstractModel from "@/App/Model/AbstractModel";
import Media from "@/App/Model/Media";

export default class Post extends AbstractModel {
    public id?: number;
    public text: string | null = null;
    public medias: Media[] = [];
    public postButtonRows: PostButtonRow[] = [];
    public scheduledAt: string | null = null;

    get scheduledAtAsDate(): Date | null {
        return this.scheduledAt !== null ? new Date(this.scheduledAt) : null;
    }

    set scheduledAtAsDate(date: Date | null) {
        this.scheduledAt = date?.toISOString() ?? null;
    }
}
