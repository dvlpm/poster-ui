import AbstractModel from "@/App/Model/AbstractModel";

export default class ButtonType extends AbstractModel {
    public name: string;

    constructor(name: string) {
        super();
        this.name = name;
    }
}
