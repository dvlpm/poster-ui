import AbstractModel from "@/App/Model/AbstractModel";

export default class Channel extends AbstractModel {
    public id: number;
    public name: string;
    public isPrivate: boolean;

    constructor(id: number, name: string, isPrivate: boolean) {
        super();
        this.id = id;
        this.name = name;
        this.isPrivate = isPrivate;
    }
}
