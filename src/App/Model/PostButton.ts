import AbstractModel from "@/App/Model/AbstractModel";
import ButtonType from "@/App/Model/ButtonType";

export default class PostButton extends AbstractModel {
    public text: string | null = null;
    public url: string | null = null;
    public type: ButtonType | null = null;

    isLink(): boolean {
        return this.type?.name === 'link';
    }
}
