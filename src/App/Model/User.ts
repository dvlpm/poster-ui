import AbstractModel from "@/App/Model/AbstractModel";

export default class User extends AbstractModel {
    public token: string;

    constructor(token: string) {
        super();
        this.token = token;
    }
}
