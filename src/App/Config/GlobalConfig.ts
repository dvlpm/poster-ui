export class MainBotConfig {
    private name: string = 'mychannelymegabot';

    get joinLink(): string {
        return 'tg://resolve?domain=' + this.name + '&start=test';
    }
}

export default class GlobalConfig {
    private static mainBot: MainBotConfig = new MainBotConfig();

    public static mainBotConfig(): MainBotConfig
    {
        return this.mainBot;
    }
}
