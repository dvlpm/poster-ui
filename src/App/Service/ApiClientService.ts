import axios, {AxiosResponse} from "axios";

export default class ApiClientService {
    get<S>(url: string, config?: any): Promise<S> {
        return axios.get(url, config).then((response: AxiosResponse) => {
            let responseData = response.data as S;

            return Promise.resolve(responseData);
        })
    }

    post<S>(url: string, data?: any, config?: any): Promise<S> {
        return axios.post(url, data, config).then((response: AxiosResponse) => {
            let responseData = response.data as S;

            return Promise.resolve(responseData);
        })
    }

    put<S>(url: string, data?: any, config?: any): Promise<S> {
        return axios.put(url, data, config).then((response: AxiosResponse) => {
            let responseData = response.data as S;

            return Promise.resolve(responseData);
        })
    }
}