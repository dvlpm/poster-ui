import {inject, injectable} from "tsyringe";
import Media from "@/App/Model/Media";
import MediaFactory from "@/App/Factory/MediaFactory";
import ApiClientService from "@/App/Service/ApiClientService";
import RequestAuthorizationConfigFactory from "@/App/Factory/RequestAuthorizationConfigFactory";

@injectable()
export default class MediaService {
    constructor(
        @inject(ApiClientService) private apiClientService: ApiClientService,
        @inject(RequestAuthorizationConfigFactory) private requestAuthorizationConfigFactory: RequestAuthorizationConfigFactory
    ) {
    }

    create(file: File, userToken: string): Promise<Media> {
        let formData = new FormData();
        formData.append("files[]", file);

        return this.apiClientService.post<Media[]>(
            `/api/medias`,
            formData,
            {
                headers: {
                    ...this.requestAuthorizationConfigFactory.create(userToken).headers,
                    "Content-Type": "multipart/form-data"
                }
            }
        ).then((medias: object[]) => {
            return MediaFactory.createArray(medias)[0];
        });
    }
}
