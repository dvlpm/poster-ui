import {inject, injectable} from "tsyringe";
import Channel from "@/App/Model/Channel";
import ChannelFactory from "@/App/Factory/ChannelFactory";
import ApiClientService from "@/App/Service/ApiClientService";
import RequestAuthorizationConfigFactory from "@/App/Factory/RequestAuthorizationConfigFactory";

@injectable()
export default class ChannelService {
    constructor(
        @inject(ApiClientService) private apiClientService: ApiClientService,
        @inject(RequestAuthorizationConfigFactory) private requestAuthorizationConfigFactory: RequestAuthorizationConfigFactory
    ) {
    }

    list(userToken: string): Promise<Channel[]> {
        return this.apiClientService.get<Channel[]>(
            '/api/channels',
            this.requestAuthorizationConfigFactory.create(userToken)
        ).then((channels: object[]) => {
            return ChannelFactory.createArray(channels);
        });
    }
}
