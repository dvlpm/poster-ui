import Post from '@/App/Model/Post';
import {inject, injectable} from "tsyringe";
import PostFactory from "@/App/Factory/PostFactory";
import OrderBy from "@/App/Dto/OrderBy";
import ApiClientService from "@/App/Service/ApiClientService";
import qs from 'qs';
import OrderByHelper from "@/App/Helper/OrderByHelper";
import RequestAuthorizationConfigFactory from "@/App/Factory/RequestAuthorizationConfigFactory";

@injectable()
export default class PostService {
    constructor(
        @inject(ApiClientService) private apiClientService: ApiClientService,
        @inject(RequestAuthorizationConfigFactory) private requestAuthorizationConfigFactory: RequestAuthorizationConfigFactory
    ) {
    }

    list(
        channelId: number,
        limit: number,
        offset: number,
        orderBy: OrderBy<PostOrderByColumn>[],
        userToken: string
    ): Promise<Post[]> {
        return this.apiClientService.get<Post[]>(
            `/api/channel/${channelId}/posts`,
            {
                ...this.requestAuthorizationConfigFactory.create(userToken),
                params: {
                    limit,
                    offset,
                    orderBy: OrderByHelper.makeArray(orderBy)
                },
                paramsSerializer: (params: object) => {
                    return qs.stringify(params, {encode: false});
                },
            }
        ).then((posts: object[]) => {
            return PostFactory.createArray(posts);
        });
    }

    save(channelId: number, post: Post, userToken: string): Promise<Post> {
        return this.apiClientService.post<Post[]>(
            `/api/channel/${channelId}/posts`,
            [post],
            this.requestAuthorizationConfigFactory.create(userToken)
        ).then((posts: object[]) => {
            return PostFactory.createArray(posts)[0];
        });
    }

    update(channelId: number, postId: number, post: Post, userToken: string): Promise<Post> {
        return this.apiClientService.put<Post>(
            `/api/channel/${channelId}/posts/${postId}`,
            post,
            this.requestAuthorizationConfigFactory.create(userToken)
        ).then((post: object) => {
            return PostFactory.create(post);
        });
    }
}

export enum PostOrderByColumn {
    id = 'id',
    scheduledAt = 'scheduledAt'
}
