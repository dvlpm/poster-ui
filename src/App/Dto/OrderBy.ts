export default class OrderBy<OrderByColumn> {
    public column: OrderByColumn;
    public direction: OrderByDirection;

    constructor(column: OrderByColumn, direction: OrderByDirection = OrderByDirection.ASC) {
        this.column = column;
        this.direction = direction;
    }
}

export enum OrderByDirection {
    ASC = 'asc',
    DESC = 'desc',
}
