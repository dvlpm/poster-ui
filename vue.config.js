module.exports = {
    chainWebpack: config => {
        config.module
            .rule('vue')
            .use('vue-loader')
            .loader('vue-loader');
        config.module
            .rule('ts')
            .use('ts-loader')
            .loader('ts-loader');
    },
    configureWebpack: {
        devServer: {
            port: 8080,
            host: '0.0.0.0',
            hot: true
        },
    }
};
